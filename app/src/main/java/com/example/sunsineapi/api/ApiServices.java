package com.example.sunsineapi.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServices {

    @GET("json")
    Call<ResponseBody> getSunRiseAndSunSet(@Query("lat") float lat, @Query("lng") float lng, @Query("date") String date);

}
