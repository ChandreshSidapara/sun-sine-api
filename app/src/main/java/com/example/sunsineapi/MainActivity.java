package com.example.sunsineapi;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.sunsineapi.api.ApiClient;
import com.example.sunsineapi.api.ApiServices;
import com.example.sunsineapi.model.LatLngModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ProgressDialog pd;
    DatePickerDialog dpd;
    EditText etLng,etlat,etDate;
    Button btnSubmit;
    ScrollView scrollViewsun;
    SimpleDateFormat sdf;
    TextView tvSunRise,tvSunset,tvsolar_noon,tv_day_length,tv_civil_twilight_begin,tv_civil_twilight_end,tv_nautical_twilight_begin,
            tv_nautical_twilight_end,tv_astronomical_twilight_begin,tv_astronomical_twilight_end;
    LatLngModel latLngModel = new LatLngModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initReference();


    }
    void initReference()
    {
        etlat = findViewById(R.id.et_latitude);
        etLng = findViewById(R.id.et_longitude);
        etDate = findViewById(R.id.etDate);
        btnSubmit = findViewById(R.id.btnSubmit);
        scrollViewsun = findViewById(R.id.SunscrollView);
        tvSunRise = findViewById(R.id.tvSunRise);
        tvSunset = findViewById(R.id.tvSunSet);
        tvsolar_noon = findViewById(R.id.tvsolar_noon);
        tv_day_length = findViewById(R.id.tvday_length);
        tv_civil_twilight_begin = findViewById(R.id.tvcivil_twilight_begin);
        tv_civil_twilight_end = findViewById(R.id.tvcivil_twilight_end);
        tv_nautical_twilight_begin = findViewById(R.id.tvnautical_twilight_begin);
        tv_nautical_twilight_end= findViewById(R.id.tvnautical_twilight_end);
        tv_astronomical_twilight_begin = findViewById(R.id.tvastronomical_twilight_begin);
        tv_astronomical_twilight_end = findViewById(R.id.tvastronomical_twilight_end);

        sdf=new SimpleDateFormat("YYYY-MM-dd");
        etDate.setText(sdf.format(new Date()));

        setDateTimeField();

        etDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dpd.show();
                return false;
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSunRiseAndSunSet();

            }
        });
    }

    void setDateTimeField(){

        Calendar newCalendar = Calendar.getInstance();
        dpd = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                etDate.setText(sdf.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR),newCalendar.get(Calendar.MONTH),newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    void showDialog()
    {
        if (pd == null)
        {
            pd = new ProgressDialog(this);
            pd.setMessage("Pleas wait");
            pd.setTitle(getString(R.string.app_name));
        }
        if (pd!= null && !pd.isShowing())
        {
            pd.show();
        }
    }

    void dissmiss()
    {
        if (pd != null && pd.isShowing())
        {
            pd.dismiss();
        }
    }
    void getSunRiseAndSunSet()
    {
        showDialog();
        ApiServices apiService =
                ApiClient.getClient().create(ApiServices.class);

        float lat= Float.valueOf(etlat.getText().toString());
        float lng =Float.valueOf(etLng.getText().toString());
        String date = etDate.getText().toString();
        Call<ResponseBody> call = apiService.getSunRiseAndSunSet(lat,lng,date);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    parseJsonResponse(response.body().string()) ;
                    scrollViewsun.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void parseJsonResponse(String jsonResponse)
    {
        try {

            JSONObject jsonObject = new JSONObject(jsonResponse);
            JSONObject jsonObject1 =jsonObject.getJSONObject("results");


            latLngModel.setSunrise(jsonObject1.getString("sunrise"));
            latLngModel.setSunset(jsonObject1.getString("sunset"));
            latLngModel.setSolar_noon(jsonObject1.getString("solar_noon"));
            latLngModel.setDay_length(jsonObject1.getString("day_length"));
            latLngModel.setCivil_twilight_begin(jsonObject1.getString("civil_twilight_begin"));
            latLngModel.setCivil_twilight_end(jsonObject1.getString("civil_twilight_end"));
            latLngModel.setNautical_twilight_begin(jsonObject1.getString("nautical_twilight_begin"));
            latLngModel.setNautical_twilight_end(jsonObject1.getString("nautical_twilight_end"));
            latLngModel.setAstronomical_twilight_begin(jsonObject1.getString("astronomical_twilight_begin"));
            latLngModel.setAstronomical_twilight_end(jsonObject1.getString("astronomical_twilight_end"));

//            latLngModeList.add(latLngModel);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        tvSunset.setText(latLngModel.getSunset());
        tvSunRise.setText(latLngModel.getSunrise());
        tvsolar_noon.setText(latLngModel.getSolar_noon());
        tv_nautical_twilight_end.setText(latLngModel.getNautical_twilight_end());
        tv_nautical_twilight_begin.setText(latLngModel.getNautical_twilight_begin());
        tv_day_length.setText(latLngModel.getDay_length());
        tv_civil_twilight_end.setText(latLngModel.getCivil_twilight_end());
        tv_civil_twilight_begin.setText(latLngModel.getCivil_twilight_begin());
        tv_astronomical_twilight_end.setText(latLngModel.getAstronomical_twilight_end());
        tv_astronomical_twilight_begin.setText(latLngModel.getAstronomical_twilight_begin());
        dissmiss();
    }
}